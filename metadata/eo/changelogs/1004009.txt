1.4.9:
 * etaj korektoj al tradukoj

1.4.8:
 * aldonis polan tradukon (danke al Verdulo)
 * aldonis Esperantan tradukon (danke al Verdulo)

1.4.7:
 * vintro baldaŭas… – aldonis novajn nivelojn
 * ideoj mankas al mi, bonvolu sendi al mi ideojn pri novaj niveloj

1.4.6:
 * plibonigita rusa traduko, danke al Dmitry
